pipeline {
	agent {
		label "mercury"
	}
	parameters {
		string defaultValue: 'latest', description: '', name: 'sync_number', trim: false
	}

	stages{		
		stage("Sync the regression results from scv01 to mercury"){
			steps{
				script{
					env.buildnumber = sh(returnStdout: true, script: "ssh scv01 'readlink -f ~/regression-${params.sync_number}' | sed 's/.*-//g'").trim()
					println(env.buildnumber)
					currentBuild.setDescription("sync_number : ${env.buildnumber}")
					sh "rm -rf ./chronos-coverage/*"
					sh "rsync -a scv01:~/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/execute ./chronos-coverage"					
				}
			}
		}
		
		stage("Sync the regression results from mercury to sun"){
			steps{
				script{
					sh "rsync -a ./chronos-coverage/execute 192.168.20.193:/home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs"					
				}
			}
		}
		/*
		stage("Sync zipline, pvt and otp"){
			agent{
				label "master"
			}

			steps{
				script{
					sh "rsync -a /home/jenkins/tmp/DevOps5/workspace/wake-zipline/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/execute /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs"
					sh "rsync -a /home/jenkins/tmp/DevOps5/workspace/wake-pvt/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/execute /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs"
					sh "rsync -a /home/jenkins/tmp/DevOps5/workspace/wake-otp/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/execute /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs"
				}
			}
		}
		*/
		stage("Merge regression coverage"){
			agent{
				label "white"
			}
			steps{
				script{
					sh """
					module load synopsys/vcs/Q-2020.03-SP1
					cd /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/compile
					cp -a /home/jenkins/Chronos/merge-coverage.sh ./
					cp -a /home/jenkins/Chronos/exclusion_wo_zipline.cfg ./
					./merge-coverage.sh
					"""
					sh (script : "cat /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt", returnStdout : false)								
				}
			}
		}
		
		stage("Show corverage graph"){
			agent{
				label "master"
			}
			steps{
				script{
					sh "echo Score > chronos-coverage-score.csv"
					sh "head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 2-6 >> chronos-coverage-score.csv"						
					plot csvFileName: 'chronos-coverage-score.csv', description: 'Score coverage', group: 'coverage', style: 'line', title: 'Score coverage', yaxis: 'percentage', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: "chronos-coverage-score.csv", inclusionFlag: 'OFF', url: '']]
					sh "sed -i 's/\"${BUILD_NUMBER}\"/\"${env.buildnumber}\"/g' /data/users/share/jenkins/jobs/Chronos_cov/chronos-coverage-score.csv"

					sh "echo Line > chronos-coverage-line.csv"
					sh "head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 9-13 >> chronos-coverage-line.csv"						
					plot csvFileName: 'chronos-coverage-line.csv', description: 'Line coverage', group: 'coverage', style: 'line', title: 'Line coverage', yaxis: 'percentage', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: "chronos-coverage-line.csv", inclusionFlag: 'OFF', url: '']]
					sh "sed -i 's/\"${BUILD_NUMBER}\"/\"${env.buildnumber}\"/g' /data/users/share/jenkins/jobs/Chronos_cov/chronos-coverage-line.csv"

					sh "echo Toggle > chronos-coverage-toggle.csv"
					sh "head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 16-20 >> chronos-coverage-toggle.csv"						
					plot csvFileName: 'chronos-coverage-toggle.csv', description: 'Toggle coverage', group: 'coverage', style: 'line', title: 'Toggle coverage', yaxis: 'percentage', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: "chronos-coverage-toggle.csv", inclusionFlag: 'OFF', url: '']]
					sh "sed -i 's/\"${BUILD_NUMBER}\"/\"${env.buildnumber}\"/g' /data/users/share/jenkins/jobs/Chronos_cov/chronos-coverage-toggle.csv"

					sh "echo Branch > chronos-coverage-branch.csv"
					sh "head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 23-27 >> chronos-coverage-branch.csv"						
					plot csvFileName: 'chronos-coverage-branch.csv', description: 'Branch coverage', group: 'coverage', style: 'line', title: 'Branch coverage', yaxis: 'percentage', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: "chronos-coverage-branch.csv", inclusionFlag: 'OFF', url: '']]
					sh "sed -i 's/\"${BUILD_NUMBER}\"/\"${buildnumber}\"/g' /data/users/share/jenkins/jobs/Chronos_cov/chronos-coverage-branch.csv"

					sh "echo Port > chronos-coverage-port.csv"
					sh "head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt_tglport/tests.txt | tail -1 | cut -c 9-13 >> chronos-coverage-port.csv"						
					plot csvFileName: 'chronos-coverage-port.csv', description: 'Port coverage', group: 'coverage', style: 'line', title: 'Port coverage', yaxis: 'percentage', csvSeries: [[displayTableFlag: false, exclusionValues: '', file: "chronos-coverage-port.csv", inclusionFlag: 'OFF', url: '']]
					sh "sed -i 's/\"${BUILD_NUMBER}\"/\"${buildnumber}\"/g' /data/users/share/jenkins/jobs/Chronos_cov/chronos-coverage-port.csv"

					sh """
					head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 2-6 > cov_data.txt
					head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 9-13 >> cov_data.txt
					head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 16-20 >> cov_data.txt
					head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt/tests.txt | tail -1 | cut -c 23-27 >> cov_data.txt
					head -n 5 /home/share/jenkins/regression-${env.buildnumber}/soc-regression-workspace/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/top_rpt_tglport/tests.txt | tail -1 | cut -c 9-13 >> cov_data.txt
					"""
					cov_data = sh (script : " cat cov_data.txt |  tr '\n' '\t' ", returnStdout : true)

					sh "rm -rf ./cov_data.txt"
					
					previous_des = currentBuild.getDescription()
					currentBuild.setDescription("${previous_des}\nScore\tLine\tToggle\tBranch\tPort\n${cov_data}")
				}
			}
		}
	}
}
